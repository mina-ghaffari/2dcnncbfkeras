import scipy.io as sio
import nibabel as nib
from nibabel.testing import data_path
import os


import numpy as np
from keras import backend as K
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D
from keras.layers.advanced_activations import LeakyReLU 
from keras.preprocessing.image import ImageDataGenerator

np.random.seed(25)


def preprocessdata (CBFmat, PatchSize):

    data = sio.loadmat (CBFmat)

    x_test  = data ['x_test']
    x_test  = x_test.reshape(x_test.shape[0], PatchSize, PatchSize, 1)
    Y_test  = np.transpose (data ['y_test'])
    Y_test  = Y_test.reshape (Y_test.shape[0],)

    x_train  = data ['x_train']
    x_train = x_train.reshape(x_train.shape[0], PatchSize, PatchSize, 1)
    Y_train  = np.transpose (data ['y_train'])

    Patchindices  = data ['patch_indices']


    number_of_classes = 2
    y_train = np_utils.to_categorical(Y_train, number_of_classes)
    y_test = np_utils.to_categorical(Y_test, number_of_classes)

    return x_train , y_train , x_test, y_test , Patchindices



def dice_coef(y_true, y_pred):
    smooth = 1.
#     y_pred = K.cast(y_pred, tf.float32)
#     y_true = K.cast(y_true, tf.float32)
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)




def get2DCnn(input_size):
    model = Sequential()

    model.add(Conv2D(32, (3, 3), padding='same' ,input_shape=(input_size,input_size,1)))
    model.add(Activation('relu'))
    BatchNormalization(axis=-1)
    model.add(Conv2D(32, (3, 3) , padding='same'))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))


    BatchNormalization(axis=-1)
    model.add(Conv2D(64,(3, 3),padding='same'))
    model.add(Activation('relu'))
    BatchNormalization(axis=-1)

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))

    model.add(MaxPooling2D(pool_size=(2,2)))


    model.add(Flatten())                  # Fully connected layer


    BatchNormalization()
    model.add(Dense(512))
    model.add(Activation('relu'))
    BatchNormalization()
    model.add(Dropout(0.8))
    model.add(Dense(2))


    model.add(Activation('sigmoid'))          #sigmoid got better results for binary classification in comparison to softmax
    model.compile(loss='binary_crossentropy', optimizer=Adam(), metrics=['accuracy'])
 #  model.compile( loss= dice_coef_loss , optimizer=Adam(lr=1e-5), metrics=[dice_coef])

    model.summary()
    return model



def train_and_predict(PatchSize, x_train, y_train, x_test, y_test ):
    gen = ImageDataGenerator()
    test_gen = ImageDataGenerator()

    train_generator = gen.flow(x_train, y_train, batch_size=64)
    test_generator = test_gen.flow(x_test, y_test, batch_size=64)

    model = get2DCnn(PatchSize)
    model.fit_generator(train_generator, steps_per_epoch=43078//64, epochs=10, 
                        validation_data=test_generator, validation_steps=39088//64)


    score = model.evaluate(x_test, y_test)
    print()
    print('Test accuracy: ', score[1])
    predictions = model.predict_classes(x_test)
    return predictions



def createMask (predictions, MaskShape0, MaskShape1, MaskShape2, PatchSize , Patchindices):
    mask = np.zeros((MaskShape0, MaskShape1 ,MaskShape2))

    cnt_i = mask.shape[0] - PatchSize +1
    cnt_j = mask.shape[1] - PatchSize +1

    Y_predicted = np.transpose(predictions)
    Label_Index = Patchindices

    i=0
    j=0
    k=0
    for l in range (0, Label_Index.shape[0] ):  

        k =  (Label_Index[l,0] // (cnt_i *cnt_j)) 

        k_res = Label_Index[l,0] - k* (cnt_i*cnt_j)

        j =  (k_res// cnt_i)
        i = k_res - j* cnt_i
        mask [int (i +(PatchSize-1)/2), int (j+(PatchSize-1)/2),k] =  Y_predicted [l]


    mask_out = mask.copy()
    for i in range (1, mask.shape[0]-1):
        for j in range (1, mask.shape[1]-1):
            for k in range (1, mask.shape[2]-1):
                if (np.count_nonzero (mask [i-1:i+1, j-1:j+1, k-1:k+1])  == 1 ):
                    mask_out [i, j, k] = 0            


    mask_out [0,:,:]  = 0
    mask_out [:,0,:]  = 0
    mask_out [:,:,0]  = 0
    mask_out [mask_out.shape[0]-1,:,:]   = 0
    mask_out [:,mask_out.shape[1]-1,:]   = 0
    mask_out [:,:,mask_out.shape[2]-1]   = 0


    img = nib.load('13_mask.nii.gz')
    imgAffine = img.affine

    img2 = nib.Nifti1Image(mask_out, imgAffine)
    nib.save(img2, '13Pred.nii')





if __name__ == '__main__':
    PatchSize = 13
    CBFmat = 'CBF.mat'
    MaskShape0 =64
    MaskShape1 =64
    MaskShape2 = 20
    x_train , y_train , x_test, y_test , Patchindices =  preprocessdata (CBFmat, PatchSize)
    predictions = train_and_predict(PatchSize, x_train, y_train, x_test, y_test )
    createMask (predictions, MaskShape0, MaskShape1, MaskShape2, PatchSize , Patchindices)